import React from 'react';

const FilmPoster = (props) => {
  return (
      <div className="film__poster">
        <div className="poster__img">
          <img src={props.url} alt={props.title}/>
        </div>
        <div className="poster__desc">
          <h3>{props.title}</h3>
          <div className="poster__year">{props.year}</div>
        </div>
      </div>
  );
};

export default FilmPoster;

