import React from 'react';

const News = (props) => {
  return (
      <div className="news">
        <h1>{props.heading}</h1>
        <p>{props.description}</p>
        <a href={props.link}>Подробнее</a>
      </div>
  );
};

export default News;