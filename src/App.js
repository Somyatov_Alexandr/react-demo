import React, { Component } from 'react';
import './App.css';
import FilmPoster from "./FilmPoster/FilmPoster";
import News from "./News/News";


class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="new_films">
          <FilmPoster
              title="Мстители: Война бесконечности"
              year="2018"
              url="https://www.kinopoisk.ru/images/film_big/843649.jpg"
          />
          <FilmPoster
              title="Batman 3"
              year="2010"
              url="https://3dnews.ru/assets/external/illustrations/2014/09/29/902635/15319122016_892732b6c9_z.jpg"
          />
          <FilmPoster
              title="Звездные войны: Последний джедай"
              year="2017"
              url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOqqyLKvPn5FBBGnfGj7lRCT9HLzRsxUH3jvWGTyH20HWSjBVv"
          />
        </div>
        <div className="news">
          <News
              heading="Заголовок 1"
              description="Какое то описание новости 1"
              link="http://google.com"
          />
          <News
              heading="Заголовок 2"
              description="Какое то описание новости 2"
              link="http://ya.ru"
          />
        </div>
      </div>
    );
  }
}

export default App;
